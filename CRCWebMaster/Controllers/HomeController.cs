﻿using CRCWebMaster.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRCWebMaster.Controllers
{
    public class HomeController : BaseController
    {
        // GET: Home
        //public ActionResult IndexCliente()
        //{
        //    return View();
        //}

        public ActionResult IndexCliente(TrackViewModel model)
        {
            return View(model);
        }

        [HttpGet]
        public ActionResult TrackingIndex(string codigo)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("IndexCliente", "Home");
            }
           var obj = context.Reclamo.FirstOrDefault(x => x.TAG == codigo);

            if (obj==null)
            {
                PostMessage(MessageType.Warning, "Codigo no encontrado");
                return RedirectToAction("IndexCliente","Home");
            }
            var viewmodel = new TrackViewModel();

            viewmodel.CargarDatos(obj);
            return View("IndexCliente", viewmodel);
          

        }
    }
}