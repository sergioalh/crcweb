﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using CRCWebMaster.Models;
using CRCWebMaster.ViewModels;

namespace CRCWebMaster.Controllers
{
    public class ReclamoController : BaseController
    {
        // GET: Reclamo
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LstReclamo(int? ProductoId)
        {
            var user = Session["UsuarioID"];
            if (user == null)
            {
                PostMessage(MessageType.Error, "Debe iniciar sesión para ver sus reclamos.");
                return View();
            }

            var list = context.Reclamo.Where(x => x.Producto.Cliente.Usuario.Username == user).ToList();

            if (ProductoId.HasValue)
            {
                list = list.Where(x => x.ProductoID == ProductoId).ToList();
            }

            if (list.Count == 0)
            {
                PostMessage(MessageType.Warning, "Aún no tiene reclamos registrados.");
                return View();
            }

            ViewBag.listReclamos = list;

            return View();
        }

        public ActionResult AddReclamoInstrumento(int? ReclamoID)
        {
            var viewModel = new RInstrumentoViewModel();

            if (ReclamoID.HasValue)
            {
                var obj = context.Reclamo.
                    FirstOrDefault(x => x.ID == ReclamoID);

                viewModel.CargarDatos(obj);
            }
            var user = Session["UsuarioID"];

            if (user!=null)
            {
                viewModel.productos = context.Producto.Where(x => x.Cliente.Usuario.Username == user).ToList();
            }

            viewModel.productoViewModel.modelos = context.Modelo.ToList();
            viewModel.motivos.Add("motivo1");
            viewModel.motivos.Add("motivo2");
            viewModel.motivos.Add("motivo3");
            

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddReclamoInstrumento(RInstrumentoViewModel model)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    model.motivos.Add("motivo1");
                    model.motivos.Add("motivo2");
                    model.motivos.Add("motivo3");

                    TryUpdateModel(model);
                    return View(model);
                }
                using (var Transaction = new TransactionScope())
                {
                    var reclamo = new Reclamo();
                    context.Reclamo.Add(reclamo);

                    if (model.ProductoID.HasValue)
                    {
                        reclamo.ProductoID = model.ProductoID;
                    }
                    else
                    {
                        var cliente = new Cliente();
                        context.Cliente.Add(cliente);
                        cliente.RazonSocial = model.clienteViewModel.RazonSocial;
                        cliente.Contacto = model.clienteViewModel.Contacto;
                        cliente.Correo = model.clienteViewModel.Correo;
                        cliente.Direccion = model.clienteViewModel.Direccion;
                        cliente.Telefono = model.clienteViewModel.Telefono;

                        context.SaveChanges();

                        var producto = new Producto();
                        context.Producto.Add(producto);
                        producto.ClienteID = cliente.ID;
                        producto.ModeloID = model.productoViewModel.ModeloID;
                        producto.Nombre = model.productoViewModel.Nombre;
                        producto.NumeroSerie = model.productoViewModel.NumSerie;
                        producto.Factura = model.productoViewModel.Factura;

                        context.SaveChanges();

                        reclamo.ProductoID = producto.ID;
                    }

                    reclamo.Descripcion = model.Descripcion;
                    reclamo.MotivoEnvio = model.MotivoEnvio;
                    reclamo.Estado = "Nuevo";
                    reclamo.FechaRegistro = DateTime.Now;
                    reclamo.TipoReclamo = "Instrumento";

                    
                    reclamo.TAG = TrackNumberGenerator.RandomString(10);


                    context.SaveChanges();
                    Transaction.Complete();
                    PostMessage(MessageType.Success, "Registro exitoso.");
                }
                
                return RedirectToAction("IndexCliente", "Home");
            }
            catch (Exception ex)
            {
                var viewModel = new RInstrumentoViewModel();
                viewModel.motivos.Add("motivo1");
                viewModel.motivos.Add("motivo2");
                viewModel.motivos.Add("motivo3");
                TryUpdateModel(model);
                return View(model);
            }
        }


        public ActionResult AddReclamoHerramienta(int? ReclamoID)
        {
            var viewModel = new RHerramientaViewModel();

            if (ReclamoID.HasValue)
            {
                var obj = context.Reclamo.
                    FirstOrDefault(x => x.ID == ReclamoID);

                viewModel.CargarDatos(obj);
            }
            var user = Session["UsuarioID"];

            if (user != null)
            {
                viewModel.productos = context.Producto.Where(x => x.Cliente.Usuario.Username == user).ToList();
            }

            viewModel.productoViewModel.modelos = context.Modelo.ToList();
            viewModel.modosdefalla.Add("modo1");
            viewModel.modosdefalla.Add("modo2");
            viewModel.modosdefalla.Add("modo3");


            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddReclamoHerramienta(RHerramientaViewModel model)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    model.modosdefalla.Add("modo1");
                    model.modosdefalla.Add("modo2");
                    model.modosdefalla.Add("moDo3");

                    TryUpdateModel(model);
                    return View(model);
                }
                using (var Transaction = new TransactionScope())
                {
                    var reclamo = new Reclamo();
                    context.Reclamo.Add(reclamo);

                    if (model.ProductoID.HasValue)
                    {
                        reclamo.ProductoID = model.ProductoID;
                    }
                    else
                    {
                        var cliente = new Cliente();
                        context.Cliente.Add(cliente);
                        cliente.RazonSocial = model.clienteViewModel.RazonSocial;
                        cliente.Contacto = model.clienteViewModel.Contacto;
                        cliente.Correo = model.clienteViewModel.Correo;
                        cliente.Direccion = model.clienteViewModel.Direccion;
                        cliente.Telefono = model.clienteViewModel.Telefono;

                        context.SaveChanges();

                        var producto = new Producto();
                        context.Producto.Add(producto);
                        producto.ClienteID = cliente.ID;
                        producto.ModeloID = model.productoViewModel.ModeloID;
                        producto.Nombre = model.productoViewModel.Nombre;
                        producto.NumeroSerie = model.productoViewModel.NumSerie;
                        producto.Factura = model.productoViewModel.Factura;

                        context.SaveChanges();

                        reclamo.ProductoID = producto.ID;
                    }

                    reclamo.Descripcion = model.Descripcion;
                    reclamo.ModoFalla = model.ModoFalla;
                    reclamo.NumeroUsos = model.NumeroUsos;
                    reclamo.Aplicacion = model.Aplicacion;
                    reclamo.Estado = "Nuevo";
                    reclamo.FechaRegistro = DateTime.Now;
                    reclamo.TipoReclamo = "Herramienta";


                    reclamo.TAG = TrackNumberGenerator.RandomString(10);


                    context.SaveChanges();
                    Transaction.Complete();
                    PostMessage(MessageType.Success, "Registro exitoso.");
                }

                return RedirectToAction("IndexCliente", "Home");
            }
            catch (Exception ex)
            {
                var viewModel = new RHerramientaViewModel();
                viewModel.modosdefalla.Add("modo1");
                viewModel.modosdefalla.Add("modo2");
                viewModel.modosdefalla.Add("modo3");
                TryUpdateModel(model);

                return View(model);
            }
        }
    }
}