﻿using CRCWebMaster.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRCWebMaster.Controllers
{
    public class LoginController : BaseController
    {
       
        public ActionResult SignIn()
        {
            return View();
        }

        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("SignIn", "Login");
        }

        [HttpPost]
        public ActionResult SignIn(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            if (model.Usuario.Equals("admin")&& model.Password.Equals("admin"))
            {
                Session["UsuarioId"] =model.Usuario;
                return RedirectToAction("IndexCliente","Home");
            }
            else
            {
                TempData["message"] = "Ingresar datos correctos.";
                return View();
            }
        }
    }
}