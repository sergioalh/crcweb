﻿using CRCWebMaster.Models;
using CRCWebMaster.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

namespace CRCWebMaster.Controllers
{
    public class ProductoController : BaseController
    {
        // GET: Producto
        public ActionResult LstProducto()
        {
            var user = Session["UsuarioID"];
            if(user==null)
            {
                PostMessage(MessageType.Error, "Debe iniciar sesión para ver sus productos.");
                return View();
            }
            var list = context.Producto.Where(x => x.Cliente.Usuario.Username == user).ToList();

            if(list.Count==0)
            {
                PostMessage(MessageType.Warning, "Aún no tiene productos registrados.");
                return View();
            }

            ViewBag.listProductos = list;

            return View();
        }

        public ActionResult AddEditProducto(int? ProductoID)
        {
            var viewModel = new ProductoViewModel();

            if (ProductoID.HasValue)
            {
                var obj = context.Producto.
                    FirstOrDefault(x => x.ID == ProductoID);

                viewModel.CargarDatos(obj);
            }
           

            viewModel.modelos = context.Modelo.ToList();


            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddEditProducto(ProductoViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    model.modelos = context.Modelo.ToList();

                    TryUpdateModel(model);
                    return View(model);
                }
                using (var Transaction = new TransactionScope())
                {
                    var producto = new Producto();
                   
                    if (model.Codigo.HasValue)
                    {
                        producto = context.Producto.
                             FirstOrDefault(x => x.ID == model.Codigo);
                    }
                    else
                    {
                        context.Producto.Add(producto);
                    }

                    var user = Session["UsuarioID"];
                    if (user == null)
                    {
                        PostMessage(MessageType.Error, "Debe iniciar sesión para registrar un producto.");
                        return View(model);
                    }

                    producto.ClienteID = context.Cliente.FirstOrDefault(x=>x.Usuario.Username==user).ID;
                    producto.ModeloID = model.ModeloID;
                    producto.Nombre = model.Nombre;
                    producto.NumeroSerie = model.NumSerie;
                    producto.Factura = model.Factura;


                    context.SaveChanges();
                    Transaction.Complete();
                    PostMessage(MessageType.Success, "Registro exitoso.");
                }

                return RedirectToAction("IndexCliente", "Home");
            }
            catch (Exception ex)
            {
                var viewModel = new ProductoViewModel();
                viewModel.modelos = context.Modelo.ToList();

                TryUpdateModel(model);
                return View(model);
            }
        }

    }
}