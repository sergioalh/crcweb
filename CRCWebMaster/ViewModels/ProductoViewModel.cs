﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using CRCWebMaster.Models;

namespace CRCWebMaster.ViewModels
{
    public class ProductoViewModel
    {
        public int? Codigo { get; set; }
        public int? ModeloID { get; set; }
        public int? ClienteID { get; set; }
        public string Factura { get; set; } = String.Empty;
        [Required]
        public string NumSerie { get; set; } = String.Empty;
        public string Nombre { get; set; } = String.Empty;
        public List<Modelo> modelos { get; set; } = new List<Modelo>();
        public List<Accesorio> accesorios { get; set; } = new List<Accesorio>();


        public void CargarDatos(Producto obj)
        {
            Codigo = obj.ID;
            Factura = obj.Factura;
            ModeloID = obj.ModeloID;
            NumSerie = obj.NumeroSerie;
            ClienteID = obj.ClienteID;
            Nombre = obj.Nombre;
            accesorios = obj.Accesorio.ToList();
        }

    }
}