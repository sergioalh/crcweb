﻿using CRCWebMaster.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRCWebMaster.ViewModels
{
    public class RHerramientaViewModel
    {
        public int? Codigo { get; set; }
        public int? ProductoID { get; set; }

        public string ModoFalla { get; set; } = String.Empty;

        [Range(0, int.MaxValue, ErrorMessage = "Ingresar un número entero válido.")]
        public int? NumeroUsos { get; set; } 

        public string Aplicacion { get; set; } = String.Empty;
        [Required]
        public string Descripcion { get; set; } = String.Empty;

        public DateTime? FechaRegistro { get; set; }

        public List<string> imagenes { get; set; } = new List<string>();
        public List<string> videos { get; set; } = new List<string>();

        public List<Producto> productos { get; set; } = new List<Producto>();
        public List<string> modosdefalla { get; set; } = new List<string>();

        public ClienteViewModel clienteViewModel { get; set; } = new ClienteViewModel();
        public ProductoViewModel productoViewModel { get; set; } = new ProductoViewModel();

        public void CargarDatos(Reclamo obj)
        {
            Codigo = obj.ID;
            ProductoID = obj.ProductoID;
            ModoFalla = obj.ModoFalla;
            NumeroUsos = obj.NumeroUsos;
            Aplicacion = obj.Aplicacion;
            Descripcion = obj.Descripcion;
            FechaRegistro = obj.FechaRegistro;
           

            productoViewModel = new ProductoViewModel
            {
                Codigo = obj.Producto.ID,
                Factura = obj.Producto.Factura,
                ModeloID = obj.Producto.ModeloID,
                NumSerie = obj.Producto.NumeroSerie,
                ClienteID = obj.Producto.ClienteID,
                Nombre = obj.Producto.Nombre
            };
            clienteViewModel = new ClienteViewModel
            {
                Codigo = obj.Producto.Cliente.Codigo,
                Contacto = obj.Producto.Cliente.Contacto,
                ID = obj.Producto.Cliente.ID,
                Correo = obj.Producto.Cliente.Correo,
                Direccion = obj.Producto.Cliente.Direccion,
                RazonSocial = obj.Producto.Cliente.RazonSocial,
                Telefono = obj.Producto.Cliente.Telefono
            };


        }
    }
}