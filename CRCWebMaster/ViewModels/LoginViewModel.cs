﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRCWebMaster.ViewModels
{
    public class LoginViewModel
    {
        [Required]
        [StringLength(20, MinimumLength = 5, ErrorMessage = "field must be atleast 6 characters")]
        public string Usuario { get; set; } = String.Empty;

        [Required]
        [StringLength(20, MinimumLength = 5, ErrorMessage = "field must be atleast 6 characters")]
        [DataType(DataType.Password)]
        public string Password { get; set; } = String.Empty;
    }
}