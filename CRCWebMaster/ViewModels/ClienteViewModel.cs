﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRCWebMaster.ViewModels
{
    public class ClienteViewModel
    {
        public int? ID { get; set; }
        public string Codigo { get; set; } = String.Empty;
      
        public string RazonSocial { get; set; } = String.Empty;
        public string Direccion { get; set; } = String.Empty;
        public string Contacto { get; set; } = String.Empty;
        public string Telefono { get; set; } = String.Empty;
        [EmailAddress]
        public string Correo { get; set; } = String.Empty;
    }
}