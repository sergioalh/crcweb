﻿using CRCWebMaster.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRCWebMaster.ViewModels
{
    public class TrackViewModel
    {
        [Required]
        [StringLength(50, MinimumLength = 5, ErrorMessage = "field must be atleast 6 characters")]
        public string TAG { get; set; } = String.Empty;
        
        public string Descripcion { get; set; } = String.Empty;

        public string MotivoEnvio { get; set; } = String.Empty;
        public string Estado { get; set; } = String.Empty;
        public string TipoReclamo { get; set; } = String.Empty;
        public string ModoFalla { get; set; } = String.Empty;
        public int? NumeroUsos { get; set; } 
        public string Aplicacion { get; set; } = String.Empty;

        public List<TrackingViewModel> trackings { get; set; } = new List<TrackingViewModel>();

        public void CargarDatos(Reclamo obj)
        {
            Descripcion = obj.Descripcion;
            MotivoEnvio = obj.MotivoEnvio;
            TAG = obj.TAG;
            Estado = obj.Estado;
            TipoReclamo = obj.TipoReclamo;
            ModoFalla = obj.ModoFalla;
            NumeroUsos = obj.NumeroUsos;
            Aplicacion = obj.Aplicacion;

            trackings = obj.Tracking.Select(x => new TrackingViewModel
            {
                Codigo = x.ID,
                FechaInicio = x.FechaInicio,
                FechaFin = x.FechaFin,
                Mensaje=x.Mensaje
            }).ToList();
        }
    }

   
}