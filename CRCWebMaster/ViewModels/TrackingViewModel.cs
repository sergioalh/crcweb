﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRCWebMaster.ViewModels
{
    public class TrackingViewModel
    {
        public int? Codigo { get; set; }
        public string Mensaje { get; set; } = String.Empty;
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
    }
}